/*Grupo:
 * Ary Sayd Sault
 * Dmitry Rocha e Silva
 * Lucas Bonafini Gonçalves 
 */

package br.ucsal.ed;

public interface IString {
	int length(); //Retorna o tamanho da string
	char charAt(int posicao); //Retorna o caracter na posio especificada
	boolean equals(String valor); //Verifica se a string passada por parmetro  igual
	boolean startsWith(String valor); //Verifica se a string inicia com o texto dado
	boolean endWith(String valor); //Verifica se a string finaliza com o texto dado
	int indexOf(char letra); //Localiza a primeira ocorrncia na string do caracter dado
	int lastIndexOf(char letra); //Localiza a ltima ocorrncia na string do caracter dado
	String substring(int inicio, int quantidadeDeCaracteres); //Retorna a substring que comea
	//em incio e tem como tamanho quantidadeDeCaracteres
	String replace(char letraASerTrocada, char letraATrocar); //Retorna uma string onde todas
	//as ocorrncias do primeiro parmetro so trocadas pelo segundo
	String concat(String valor); //Concatena a string com aquela passada como parmetro
	void imprime(); //Imprime a String
}
