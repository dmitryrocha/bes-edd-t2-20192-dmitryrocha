/*Grupo:
 * Ary Sayd Sault
 * Dmitry Rocha e Silva
 * Lucas Bonafini Gonçalves 
 */

package br.ucsal.ed;
import java.util.Scanner;

public class String implements IString {
	char[] valores;
	
	public String(int tamanho) {
		valores = new char[tamanho];
	}

	public String() {
		Scanner entrada = new Scanner(System.in);
		valores = entrada.nextLine().toCharArray();
	}

	public int length() {
		int tamanho = valores.length;
		return tamanho;
	}

	public char charAt(int posicao) {
		return valores[posicao];
	}

	public boolean equals(String valor) {
		boolean valorIgual = true;
		if(valores.length != valor.length()) {
			valorIgual = false;
			return valorIgual;
		} else {
			for (int i = 0; i < valor.length(); i++) {
				if(valor.valores[i] == valores[i]) {
					valorIgual = true;
				} else {
					valorIgual = false;
					return valorIgual;
				}
			}
		}
		return valorIgual;
	}

	public boolean startsWith(String valor) {
		boolean valorIgual = true;
		if(valores.length < valor.length()) {
			valorIgual = false;
			return valorIgual;
		} else {
			for (int i = 0; i < valor.length(); i++) {
				if(valor.valores[i] == valores[i]) {
					valorIgual = true;
				} else {
					valorIgual = false;
					return valorIgual;
				}
			}
			return valorIgual;
		}

	}

	public boolean endWith(String valor) {
		boolean valorIgual = true;
		if(valores.length < valor.length()) {
			valorIgual = false;
			return valorIgual;
		} else {
			for (int i = valor.length(), posicao = valores.length; i > 0; i--, posicao--) {
				if(valor.valores[i-1] == valores[posicao-1]) {
					valorIgual = true;
				} else {
					valorIgual = false;
					return valorIgual;
				}
			}
			return valorIgual;
		}

	}

	public int indexOf(char letra) {
		int posicao = -1;
		for (int i = 0; i < valores.length; i++) {
			if(letra == valores[i]) {
				posicao = i;
				return posicao;
			}
		}
		return posicao;
	}

	public int lastIndexOf(char letra) {
		int posicao = -1;
		for (int i = 0; i < valores.length; i++) {
			if(letra == valores[i]) {
				posicao = i;
			}
		}
		return posicao;
	}

	public String substring(int inicio, int quantidadeDeCaracteres) {
		int j = 0;
		int w = inicio;
		String sub = new String(quantidadeDeCaracteres);
		for (int i = 0; i < quantidadeDeCaracteres; i++) {
			sub.valores[j] = valores[w];
			j++;
			w++;
		}
		return sub;
	}

	public String replace(char letraASerTrocada, char letraATrocar) {
		for (int i = 0; i < valores.length; i++) {
			if(valores[i] == letraASerTrocada) {
				valores[i] = letraATrocar;
			}
		}
		return this;
	}

	public String concat(String valor) {
		String conc = new String(valores.length + valor.length());
		int num = 0;
		for (int i = 0; i < valores.length; i++) {
			conc.valores[i] = valores[i];
		}
		for (int i = valores.length; i < conc.length(); i++, num++) {
			conc.valores[i] = valor.valores[num];
		}
		return conc;
	}

	public void imprime() {
		//System.out.print("A palavra : ");
		for (int i = 0; i < valores.length; i++) {
			System.out.print(valores[i]);
		}
	}



}
