/*Grupo:
 * Ary Sayd Sault
 * Dmitry Rocha e Silva
 * Lucas Bonafini Gonçalves 
 */

package br.ucsal.ed;

import java.util.Scanner;

public class Teste {

	public static void main(java.lang.String[] args) {
		System.out.println("Digite uma palavra para testar");
		executa(new String());

	}

	public static void executa(IString texto) {
		Scanner sc = new Scanner(System.in);
		// testar length
		System.out.println("A palavra-teste tem " + texto.length() + " letras");

		// testar charAt
		System.out.println("Escolha um número maior ou igual a zero para retornar a letra na posição respectiva: ");
		int posicao = sc.nextInt();
		while (posicao < 0 || posicao > (texto.length() - 1)) {
			System.out
					.println("A posição escolhida não está dentro do tamanho da palavra-teste. Favor tentar novamente");
			posicao = sc.nextInt();
		}
		System.out.println("A letra na posição " + posicao + " é " + texto.charAt(posicao));

		// testar equals
		System.out.println("Digite uma palavra para saber se ela é igual à palavra-teste");
		String igual = new String();
		boolean vf = texto.equals(igual);
		if (vf) {
			System.out.println("A palavra é igual à palavra-teste ");
		} else {
			System.out.println("A palavra não é igual à palavra-teste");
		}

		// testar startsWith
		System.out.println("Digite um pedaço de uma palavra para saber se começa igual à palavra-teste");
		String comeca = new String();
		vf = texto.startsWith(comeca);
		if (vf) {
			System.out.println("A palavra tem o mesmo início da palavra-teste");
		} else {
			System.out.println("A palavra não tem o mesmo início da palavra-teste");
		}

		// testar endWith
		System.out.println("Digite um pedaço de uma palavra para saber se termina igual à palavra-teste");
		String termina = new String();
		vf = texto.endWith(termina);
		if (vf) {
			System.out.println("A palavra tem o mesmo fim da palavra-teste");
		} else {
			System.out.println("A palavra não tem o mesmo fim da palavra-teste");
		}

		// testar indexOf
		System.out.println("Escolha um caractere para saber qual a primeira posição onde ela aparece");
		String indOf = new String();
		char letra = indOf.charAt(0);
		posicao = texto.indexOf(letra);
		if (posicao == -1) {
			System.out.println("A letra não aparece na palavre-teste");
		} else {
			System.out.println("A letra '" + letra + "' aparece na posição " + posicao);
		}

		// testar lastIndexOf
		System.out.println("Escolha um caractere para saber qual a última posição onde ela aparece");
		String endOf = new String();
		letra = endOf.charAt(0);
		posicao = texto.lastIndexOf(letra);
		if (posicao == -1) {
			System.out.println("A letra não aparece na palavre-teste");
		} else {
			System.out.println("A letra '" + letra + "' aparece pela última vez na posição " + posicao);
		}
		
		// testar substring
		System.out.println("Para criar uma nova palavra baseada na palavra-teste, primeiro indique a posição de início:");
		posicao = sc.nextInt();
		System.out.println("Agora indique o tamanho da nova palavra");
		int tamanho = sc.nextInt();
		String subst = texto.substring(posicao, tamanho);
		System.out.print("A nova palavra é ");
		subst.imprime();
		System.out.println(" ");
		
		// testar replace
		System.out.println("Para substituir uma letra na palavra-teste, escolha uma letra:");
		String troca = new String();
		letra = troca.charAt(0);
		System.out.println("Agora escolha uma letra da palavra teste para ser substituida:");
		String troca2 = new String();
		char letra2 = troca2.charAt(0);
		texto.replace(letra, letra2);
		System.out.print("A palavra agora é ");
		texto.imprime();
		System.out.println(" ");
		
		// testar concat
		System.out.println("Para adicionar uma palavra à palavra-teste, digite a palavra a ser adicionada:");
		String conc = new String();
		texto = texto.concat(conc);
		texto.imprime();
	}

}
