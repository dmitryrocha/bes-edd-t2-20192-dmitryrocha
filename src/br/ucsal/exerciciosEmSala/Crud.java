package br.ucsal.exerciciosEmSala;

public class Crud {

	public static String[] vetorzao = new String[10];

	public static void main(String[] args) {
		executa();
	}
	
	public static void executa() {

		create("a");
		read();
		System.out.println();

		create("b");
		create("c");
		create("d");
		create("e");
		read();
		System.out.println();

		update(3,"x");
		read();
		System.out.println();

		delete(3);
		read();

	}
	
	public static void create (String letra) {
		int count = 0;
		boolean continua = true;
		while(continua){
			if(count == vetorzao.length){
				System.out.println("\nVetor lotado\n");
				continua = false;
			} else {
				if (vetorzao[count] == null) {
					vetorzao[count] = letra;
					continua = false;
				}
			}
			count++;
		}


	}

	public static void read () {
		for(int i = 0; i< vetorzao.length; i++){
			System.out.print(vetorzao[i]+"  ");
		}
	}

	public static void update (int posicao, String letra){
		vetorzao[posicao] = letra;

	}

	public static void delete(int posicao){
		for(int i = posicao; i<vetorzao.length-1; i++){
			vetorzao[i] = vetorzao[i+1];
		}

	}


}
