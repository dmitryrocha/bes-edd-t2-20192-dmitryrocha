package br.ucsal.bes20192.lista04;

public class Questao01 {
	
	public static long potencia(int n, int x) {
		if (n==0)
			return 1;
		if(n==1)
			return x;
		else
			return x*potencia(n-1, x);
	}

}
