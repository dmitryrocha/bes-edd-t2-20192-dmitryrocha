package Pilha;

public interface iPilha {
    void push(int valor);
    No pop();
    void imprimir();
}
