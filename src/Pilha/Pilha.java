package Pilha;

public class Pilha implements iPilha {

    No topo = null;
    @Override
    public void push(int valor) {
        No novo = new No();
        novo.valor = valor;
        if(topo == null) {
            topo = novo;
        } else {
            novo.prox = topo;
            topo = novo;
        }

    }

    @Override
    public No pop() {
        topo = topo.prox;
        return topo;
    }

    @Override
    public void imprimir() {
        if(topo==null) {
            System.out.println("Lista vazia!");
        }else {
            No aux = topo;
            while(aux!=null) {
                System.out.print(aux.valor + " ");
                aux=aux.prox;
            }
        }
        System.out.println("");
    }
}
