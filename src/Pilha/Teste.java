package Pilha;

public class Teste {
    public static void main(String[] args) {

        executa(new Pilha());

    }

    public static void executa(iPilha pilha) {
        pilha.push(1);
        pilha.push(4);
        pilha.push(2);
        pilha.push(7);
        pilha.push(9);

        pilha.imprimir();

        pilha.pop();
        System.out.println("Tirou o 9");
        pilha.imprimir();

        pilha.pop();
        System.out.println("Tirou o 7");
        pilha.imprimir();

        pilha.pop();
        System.out.println("Tirou o 2");
        pilha.imprimir();

        pilha.pop();
        System.out.println("Tirou o 4");
        pilha.imprimir();

        pilha.push(99);
        System.out.println("Adicionou 99");
        pilha.imprimir();

        pilha.pop();
        System.out.println("Tirou o 99");
        pilha.imprimir();

        pilha.pop();
        System.out.println("Tirou o 1");
        pilha.imprimir();



    }
}
