package listaDeExercicios06;

public class Fila implements IFila {
    No inicio = null;
    No fim = null;

    @Override
    public void inserir(int valor) {
        No novo = new No();
        novo.valor = valor;
        if(inicio == null) {
            inicio = novo;
            fim = novo;
        } else {
            fim.prox = novo;
            fim = novo;
            fim.prox = null;
        }
    }

    @Override
    public No remover() {

        if(inicio != null) {
            inicio = inicio.prox;
        }

        return inicio;
    }

    @Override
    public void imprimir() {
        if(inicio==null) {
            System.out.println("Lista vazia!");
        }else {
            No aux = inicio;
            while(aux!=null) {
                System.out.print(aux.valor + " ");
                aux=aux.prox;
            }
        }
        System.out.println("");
    }
}
