package listaDeExercicios06;

public class LdE06 {
    public static void main(String[] args) {
        executa(new Fila());
    }

    public static void executa(IFila filinha) {
        filinha.inserir(4);
        filinha.inserir(3);
        filinha.inserir(6);
        filinha.inserir(8);
        filinha.inserir(23);
        filinha.inserir(44);
        filinha.inserir(29);

        System.out.println("Questão 01:\nO tamanho da fila é "+questao01(filinha)+"\n");

        System.out.println("Questão 02:\nA fila de entrada é:");
        filinha.imprimir();
        System.out.println("A nova fila é:");
        filinha = questao02(filinha);
        filinha.imprimir();

        Fila flaTetra = new Fila();
        flaTetra.inserir(80);
        flaTetra.inserir(82);
        flaTetra.inserir(73);
        flaTetra.inserir(92);

        System.out.println("\nQuestão 03:\nA fila 1 é:");
        filinha.imprimir();
        System.out.println("A fila 2 é: ");
        flaTetra.imprimir();
        System.out.println("A fila resultante é:");
        flaTetra = questao03(filinha,flaTetra);
        flaTetra.imprimir();

        System.out.println("\nQuestão 04:\nA fila que entra é:");
        flaTetra.imprimir();
        System.out.println("A fila que sai é:");
        flaTetra = questão04(flaTetra);
        flaTetra.imprimir();





    }

    private static int questao01(IFila fila) {
        Fila filaAux = new Fila();
        filaAux = (Fila) fila;
        int tamanho = 0;
        if(filaAux.inicio == null) {
            return tamanho;
        } else {
            No aux = filaAux.inicio;
            while (aux != null) {
                aux = aux.prox;
                tamanho++;
            }
        }
        return tamanho;
    }

    private static Fila questao02(IFila fila) {
        Fila filaAux = (Fila) fila;
        int[] vetor = new int[5];
        int count = 0;
        while(count <= 4){
            vetor[count] = filaAux.inicio.valor;
            filaAux.remover();
            count++;
        }
        for(int i =4; i >= 0; i--) {
            filaAux.inserir(vetor[i]);
        }

        return filaAux;

    }
    private static Fila questao03(IFila filaNormal, Fila filaPreferencial) {
        Fila filaAux = (Fila) filaNormal;
        Fila filaRetorno = new Fila();
        int count = 0;
        No auxFilaNormal = filaAux.inicio;
        No auxFilaPreferencial = filaPreferencial.inicio;
        while(auxFilaNormal != null || auxFilaPreferencial != null) {
            if(count < 3){
                if(auxFilaNormal != null){
                    filaRetorno.inserir(auxFilaNormal.valor);
                    auxFilaNormal = auxFilaNormal.prox;
                    count++;
                } else {
                    if(auxFilaPreferencial != null){
                        filaRetorno.inserir(auxFilaPreferencial.valor);
                        auxFilaPreferencial = auxFilaPreferencial.prox;
                    }
                    count = 0;
                }
            } else {
                filaRetorno.inserir(auxFilaPreferencial.valor);
                auxFilaPreferencial = auxFilaPreferencial.prox;
                count = 0;
            }

        }
        return filaRetorno;
    }

    private static Fila questão04(IFila filinha) {
        Fila filaAux = (Fila) filinha;
        Fila filaRetorno = new Fila();
        int tamanho = questao01(filinha);
        int[] vetor = new int[tamanho];
        No aux = filaAux.inicio;

        for(int i= tamanho; i > 0; i--){
            vetor[i-1] = aux.valor;
            aux = aux.prox;
        }

        for (int i = 0; i<tamanho; i++) {
            filaRetorno.inserir(vetor[i]);
        }

        return filaRetorno;
    }
}
