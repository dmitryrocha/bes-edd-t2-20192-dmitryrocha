package ExerciciosEmSala;

import br.ucsal.ed.String;

public class Recursividade {

	public static void main(String[] args) {
		System.out.println(fatorial(5));
		System.out.println(fatorial2(5));
		System.out.println(somaRecursiva(5));
		System.out.println(potencia(3,2));

	}

	public static int fatorial(int n) {
		int fat = 1;
		if (n == 0) {
			return 1;
		} else {
			for (int i = 1; i <= n; i++) {
				fat = fat * i;

			}
			return fat;
		}
	}

	public static int fatorial2(int n) {
		if (n == 0)
			return 1;
		else
			return n * fatorial2(n - 1);
	}
	
	public static int somaRecursiva(int n) {
		if(n == 0)
			return 0;
		if(n == 1)
			return 1;
		else
			return n+somaRecursiva(n-1);
	}
	
	public static long potencia(int n, int x) {
		if (n==0)
			return 1;
		if(n==1)
			return x;
		else
			return x*potencia(n-1, x);
	}

}
