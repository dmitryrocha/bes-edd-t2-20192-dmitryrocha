package Treinando;

public class ListaDuplamenteEncadeada {
    No inicio = null;

    public void inserir(int valor){
        No novo = new No();
        novo.valor = valor;

        if(inicio == null){
            inicio = novo;
        } else {
            novo.prox = inicio;
            inicio.ant = novo;
            inicio = novo;
        }
    }

    public void inserirOrdenado(int valor) {
        No novo = new No();
        novo.valor = valor;

        if(inicio == null) {
            inicio = novo;
        } else {
            No ant = null;
            No aux = inicio;

            while(aux.prox!=null && aux.valor<valor){
                ant = aux;
                aux = aux.prox;
            }

            if(ant==null && novo.valor<aux.valor){
                novo.prox=inicio;
                inicio=novo;
            } else if (aux.prox==null && novo.valor>aux.valor){
                aux.prox=novo;
            } else {
                ant.prox=novo;
                novo.prox=aux;
            }
        }
    }

    public void remover (int chave) {
        if(inicio == null) {
            System.out.println("A lista está vazia");
        } else {
            No aux = inicio;

            while(aux.prox!=null && aux.valor!=chave){
                aux = aux.prox;
            }

            if(aux.valor == chave) {

                if(aux.ant == null) {
                    inicio = inicio.prox;
                    inicio.ant = null;
                } else if (aux.prox == null) {
                    aux.ant.prox = null;
                    aux.ant = null;
                    aux = null;
                } else {
                    aux.ant.prox = aux.prox;
                    aux.prox.ant = aux.ant;
                    aux.ant = null;
                    aux.prox = null;
                    aux = null;
                }
            } else {
                System.out.println("Objeto não encontrado!");
            }
        }
    }

    public No buscar(int chave) {
        if(inicio == null) {
            System.out.println("A lista está vazia");
        } else {
            No aux = inicio;
            while(aux.prox!=null && aux.valor!=chave) {
                aux = aux.prox;
            }
            if(aux.valor == chave) {
                return aux;
            } else {
                System.out.println("Elemento não encontrado");
            }
        }
        return null;
    }

    public void alterar(int chave, int novoValor){
        if(inicio == null){
            System.out.println("A lista está vazia!");
        } else {
            No aux = inicio;
            while(aux.prox!=null && aux.valor!=chave) {
                    aux = aux.prox;
            }
            if(aux.valor == chave) {
                aux.valor = novoValor;
            } else {
                System.out.println("Elemento não encontrado");
            }
        }
    }

    public void imprimir(){
       if(inicio == null){
           System.out.println("A lista está vazia!");
       } else {
           No aux = inicio;
           while (aux!=null){
               System.out.print(aux.valor+" ");
               aux = aux.prox;
           }
           System.out.println(" ");
       }
    }

    public void imprimirDecrescente() {
        if(inicio == null){
            System.out.println("A lista está vazia!");
        } else {
            No aux = inicio;
            while(aux.prox != null){
                aux = aux.prox;
            }
            while (aux != null) {
                System.out.print(aux.valor+" ");
                aux = aux.ant;
            }
            System.out.println(" ");
        }
    }

    public ListaDuplamenteEncadeada ordenarLista (){
        ListaDuplamenteEncadeada listaRetorno = new ListaDuplamenteEncadeada();
        if(inicio == null) {
            System.out.println("A lista enviada está vazia");
            return null;
        } else {
            int tamanho = tamanho();
            int[] vetor = new int[tamanho];
            No aux = inicio;
            for(int i = 0; i < vetor.length; i++) {
                vetor[i] = aux.valor;
                aux = aux.prox;
            }
            aux = inicio;
            for(int i=0; i < vetor.length; i++){
                for (int j=i; j>0 && vetor[j]<vetor[j-1]; j--) {
                    int temp = vetor[j-1];
                    vetor[j-1]=vetor[j];
                    vetor[j]=temp;
                }
            }
            for(int i=0; i<vetor.length; i++){
                listaRetorno.inserir(vetor[i]);
            }
            return listaRetorno;
        }
    }

    public int tamanho() {
        int count = 0;
        if(inicio == null) {
            System.out.println("A lista está vazia");
            return -1;
        } else {
            No aux = inicio;
            while(aux!=null){
                count++;
                aux=aux.prox;
            }
        }
        return count;
    }
}
