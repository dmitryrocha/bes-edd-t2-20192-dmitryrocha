package Treinando;

public class Main {
    private static String linhas = "\n-------------------------\n";
    public static void main(String[] args) {
        executa(new ListaSimplesmenteEncadeada());
        executa2(new ListaDuplamenteEncadeada());
        executa3(new ListaCircular());
    }

    public static void executa (ListaSimplesmenteEncadeada lista1) {


        lista1.inserir(1);
        lista1.inserir(4);
        lista1.inserirOrdenado(2);
        lista1.inserirOrdenado(10);
        lista1.inserirOrdenado(6);

        System.out.println("Imprimir os números inseridos");
        lista1.imprimir();
        System.out.print(linhas);

        System.out.println("Tentar remover um número que não está na lista");
        lista1.remover(3);
        System.out.print(linhas);
        System.out.println("Tentar remover o número 10");
        lista1.remover(10);
        lista1.imprimir();
        System.out.print(linhas);

        System.out.println("Trocando o 2 pelo 3");
        lista1.alterar(2,3);
        lista1.imprimir();

        System.out.print(linhas);
    }

    public static void executa2(ListaDuplamenteEncadeada lista2) {
        System.out.println("Começando testes da lista duplamente encadeada");

        lista2.inserir(3);
        lista2.inserir(6);
        lista2.inserir(23);
        lista2.inserir(37);
        lista2.inserir(22);
        lista2.inserir(17);

        System.out.println("Imprimir os números inseridos na ordem que foram adicionados:");
        lista2.imprimirDecrescente();

        System.out.println("Imprimir os número na ordem contrária à inserção:");
        lista2.imprimir();

        System.out.println("Removendo o número 22 da lista");
        lista2.remover(22);
        lista2.imprimir();

        System.out.println("Alterando o número 17 por 13");
        lista2.alterar(17,13);
        lista2.imprimir();

        System.out.println("Buscando pelo número 23");
        No noTeste = lista2.buscar(23);
        System.out.println("O valor encontrado é "+noTeste.valor);

        System.out.println("Colocando a lista em ordem crescente:");
        lista2 = lista2.ordenarLista();
        lista2.imprimirDecrescente();

        System.out.println("Zerar a lista e adicionar novos números de maneira ordenada");
        lista2.remover(3);
        lista2.remover(6);
        lista2.remover(13);
        lista2.remover(23);
        //lista2.remover(37);
        lista2.inserirOrdenado(3);
        lista2.inserirOrdenado(5);
        lista2.inserirOrdenado(1);
        lista2.inserirOrdenado(55);
        lista2.inserirOrdenado(12);
        lista2.inserirOrdenado(32);
        lista2.imprimir();
        System.out.println(linhas);


    }

    public static void executa3(ListaCircular lista3){
        System.out.println("Começar o teste da lista circular:");
        lista3.inserir(3);
        lista3.inserir(36);
        lista3.inserir(23);
        lista3.inserir(54);
        lista3.inserir(12);
        lista3.inserir(9);
        lista3.inserir(1);
        lista3.imprimir();

        System.out.println("Removendo o número 54:");
        lista3.remover(54);
        lista3.imprimir();

        System.out.println("Trocando o número 12 pelo 13");
        lista3.alterar(12,13);
        lista3.imprimir();

        System.out.println("Buscando o número 9");
        No busca = lista3.buscar(9);
        System.out.println("Número encontrado foi "+busca.valor);

        System.out.println("Imprimindo duas vezes");
        lista3.imprimirDuasVezes();
    }
}
