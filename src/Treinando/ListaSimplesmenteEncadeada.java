package Treinando;

public class ListaSimplesmenteEncadeada {

    No inicio = null;

    public void inserir(int valor) {

        No novo = new No();
        novo.valor = valor;

        if(inicio==null) {
            inicio = novo;
        }else {
            No aux = inicio;

            while(aux.prox!=null) {
                aux = aux.prox;
            }

            aux.prox = novo;
        }

    }

    public void inserirOrdenado(int valor) {
        No novo = new No();
        novo.valor = valor;

        if(inicio==null) {
            inicio = novo;
        }else {
            No ant = null;
            No aux = inicio;
            while(aux.prox!=null&&aux.valor<valor) {
                ant=aux;
                aux=aux.prox;
            }

            if(ant==null&&novo.valor<aux.valor) {
                novo.prox=inicio;
                inicio=novo;
            }else if(aux.prox==null&&novo.valor>aux.valor) {
                aux.prox=novo;
            }else {
                ant.prox=novo;
                novo.prox=aux;
            }

        }
    }

    public void remover (int valor) {
        if(inicio==null){
            System.out.println("A lista está vazia.");
        } else {
            No anterior = null;
            No aux = inicio;

            while(aux.prox!=null && aux.valor!=valor) {
                anterior = aux;
                aux = aux.prox;
            }

            if(aux.valor == valor) {
                if(anterior == null) {
                    inicio = inicio.prox;
                } else if(aux.prox == null) {
                    anterior.prox = null;
                    aux = null;
                } else {
                    anterior.prox = aux.prox;
                    aux = null;
                }
            } else {
                System.out.println("O elemento não foi encontrado");
            }
        }
    }

    public No buscar(int chave) {
        if(inicio == null) {
            System.out.println("A lista está vazia!");
        } else {
            No aux = inicio;
            while(aux.prox!=null && aux.valor!=chave) {
                aux = aux.prox;
            }
            if(aux.valor == chave) {
                return aux;
            }
        }
        return null;
    }

    public void alterar (int chave, int novoValor) {
        if(inicio == null) {
            System.out.println("A lista está vazia");
        } else {
            No aux = inicio;
            while(aux.prox!=null && aux.valor!=chave){
                aux = aux.prox;
            }
            if(aux.valor==chave){
                aux.valor = novoValor;
            } else {
                System.out.println("O valor não se encontra na lista");
            }
        }
    }

    public void imprimir() {
        if(inicio==null) {
            System.out.println("A lista está vazia");
        } else {
            No aux = inicio;

            while(aux!=null) {
                System.out.print(aux.valor+" ");
                aux = aux.prox;
            }
        }
    }

}
