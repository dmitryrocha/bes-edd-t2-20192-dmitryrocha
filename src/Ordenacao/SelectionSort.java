package Ordenacao;

public class SelectionSort {
    public static void main(String[] args) {
        executa();
    }

    public static void executa(){
        int[] seq = {7, 0, 4, 9, 6, 1, 2};
        int[] vetorOrdenado = ordenatorTabajara(seq);
        imprimir(vetorOrdenado);

    }

    public static int[] ordenatorTabajara(int[] seq) {
        int menor;

        for(int i = 0; i < seq.length; i++){
            menor = i;
            for(int j = i; j<seq.length; j++){
                if(seq[j]<seq[menor]){
                    menor = j;
                }
            }
            int temp = seq[i];
            seq[i] = seq[menor];
            seq[menor] = temp;

        }
        return seq;
    }

    public static void imprimir (int[] seq){
        for(int i = 0; i < seq.length; i++){
            System.out.print(seq[i]+" ");
        }
    }
}
