package Ordenacao;

public class ContagemBuscas {

    public static void main(String[] args) {
        executa();
    }

    public static void executa() {
        int[] seq = {7, 0, 4, 9, 6, 1, 2};
        int contador = buscaSeq(seq,4);
        System.out.println("Na busca sequencial deu "+contador);
        contador = buscaBinaria(seq,4);
        System.out.println("Na busca binária deu "+ contador);
    }

    public static int buscaSeq(int[]vet, int numeroDesejado){
        int index = -1;
        int count = 0;
        for(int i=0; i<vet.length; i++){
            count++;
            if(vet[i] == numeroDesejado){
                index = i;
            }
        }
        return count;
    }


    public static int buscaBinaria(int[] vetor, int numeroDesejado) {
        int inicio = 0;
        int fim = vetor.length-1;

        int index = -1;
        int count = 0;

        while(inicio <= fim) {
            count++;
            int meio = ((fim - inicio)/2)+inicio;
            if(vetor[meio]==numeroDesejado){
                index = meio;
                inicio = fim+1;

            } else if(numeroDesejado < vetor[meio]){
                fim = meio-1;
            } else if (numeroDesejado > vetor[meio]){
                inicio = meio+1;
            }
        }
        return count;
    }
}


