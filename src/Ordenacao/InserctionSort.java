package Ordenacao;

public class InserctionSort {
    public static void main(String[] args) {
        executa();
    }

    public static void executa() {
        int[] seq = {7,0,4,9,6,1,2};
        int[] vetor = ordenatorTabajara(seq);
        SelectionSort.imprimir(vetor);
    }

    public static int[] ordenatorTabajara(int[] vetor){
        for(int i = 0; i<vetor.length; i++){
            for(int j = i; j >0 && vetor[j]<vetor[j-1] ; j--) {

                    int temp = vetor[j - 1];
                    vetor[j-1] = vetor[j];
                    vetor[j] = temp;
            }
        }
        return vetor;
    }

}
