package listaSimplesEncadeada;

public class TesteLSE {

	public static void main(String[] args) {
		
		executa(new LSE());

	}

	public static void executa(ILSE lista) {
		lista.inserir(2);
		lista.inserir(9);
		lista.inserir(3);
		lista.inserir(0);
		lista.inserir(7);
		lista.inserir(45);
		lista.inserir(27);
		lista.inserir(32);		
		lista.imprimir();
		No no1 = lista.buscar(2);
		if(no1!=null) {
			System.out.println(no1.valor);
		}else {
			System.out.println("Chave não encontrada!");
		}
		
		No no2 = lista.buscar(0);
		if(no2!=null) {
			System.out.println(no2.valor);
		}else {
			System.out.println("Chave não encontrada!");
		}

		No no3 = lista.buscar(7);
		if(no3!=null) {
			System.out.println(no3.valor);
		}else {
			System.out.println("Chave não encontrada!");
		}

		No no4 = lista.buscar(12);
		if(no4!=null) {
			System.out.println(no4.valor);
		}else {
			System.out.println("Chave não encontrada!");
		}

		lista.alterar(2, 14);
		lista.imprimir();
		
		lista.alterar(3, 13);
		lista.imprimir();

		lista.alterar(7, 17);
		lista.imprimir();

		lista.remover(14);
		lista.imprimir();
		
		lista.remover(17);
		lista.imprimir();		
		
		lista.remover(32);
		lista.imprimir();		

	}
	
}
