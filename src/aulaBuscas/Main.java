package aulaBuscas;

public class Main {

	public static void main(String[] args) {
		executa();

	}
	
	public static void executa() {
		int[] vetor = {0,2,3,5,7,8,9,10,15};

		System.out.println(buscaSeq(vetor, 5));
		System.out.println();
		System.out.println(buscaBinaria(vetor,5));
		System.out.println();
		System.out.println(bBRecursiva(vetor,5,0,vetor.length-1));
		System.out.println();
		System.out.println(buscaSeq(vetor,8));
		System.out.println();
		System.out.println(buscaBinaria(vetor,8));
		System.out.println();
		System.out.println(bBRecursiva(vetor,8,0,vetor.length-1));
		System.out.println();
		System.out.println(buscaSeq(vetor, 33));
		System.out.println();
		System.out.println(buscaBinaria(vetor,33));
		System.out.println();
		System.out.println(bBRecursiva(vetor,33,0, vetor.length-1));
	}

	public static int buscaSeq(int[]vet, int numeroDesejado){
		int index = -1;
		for(int i=0; i<vet.length; i++){
			if(vet[i] == numeroDesejado){
				index = i;
			}
		}
		return index;
	}


	public static int bBRecursiva(int[]vet, int numeroDesejado, int inicio, int fim){
		int index = -1;
		if(fim<inicio){
			return index;
		}

		int meio = ((fim-inicio)/2)+inicio;

		if(vet[meio] == numeroDesejado){
			index = meio;
		} else {
			if(numeroDesejado < vet[meio]) {
				return bBRecursiva(vet, numeroDesejado, inicio, meio-1);
			} else {
				return bBRecursiva(vet, numeroDesejado, meio+1, fim);
			}
		}

		return index;

	}

	
	public static int buscaBinaria(int[] vetor, int numeroDesejado) {
		int inicio = 0;
		int fim = vetor.length-1;

		int index = -1;

		while(inicio <= fim) {
			int meio = ((fim - inicio)/2)+inicio;
			if(vetor[meio]==numeroDesejado){
				index = meio;
				inicio = fim+1;

			} else if(numeroDesejado < vetor[meio]){
				fim = meio-1;
			} else if (numeroDesejado > vetor[meio]){
				inicio = meio+1;
			}
		}
		return index;
	}

}
