package ListaMultidimensional;

public class LMD implements ILMD {

    Noc inicio = null;

    @Override
    public void insere(int codigo, String descricao) {

        Noc nocNovo = new Noc();
        nocNovo.codigo = codigo;
        nocNovo.descricao = descricao;
        nocNovo.ini = null;

        if(inicio == null) {
            inicio = nocNovo;
        } else {
            Noc nocAux = inicio;
            while(nocAux.prox != null) {
                nocAux = nocAux.prox;
            }

            nocAux.prox = nocNovo;
        }

    }

    @Override
    public void remove(int codigo) {
        if(inicio==null) {
            System.out.println("Lista vazia!");
        } else {
            Noc nocAux = inicio;
            Noc nocAnt = null;
            while (nocAux.prox != null && nocAux.codigo != codigo) {
                nocAnt = nocAux;
                nocAux = nocAux.prox;
            }
            if(nocAux.codigo == codigo){
                if(nocAnt == null){
                    inicio = inicio.prox;
                } else if(nocAux.prox == null) {
                    nocAnt.prox = null;
                    nocAux = null;
                } else {
                    nocAnt.prox = nocAux.prox;
                    nocAux = null;
                }
            } else {
                System.out.println("Categoria não encontrada!");
            }
        }
    }

    @Override
    public Noc buscar(int codigo) {
        if(inicio==null) {
            System.out.println("Lista vazia!");
        }else {
            Noc nocAux = inicio;
            while(nocAux.prox!=null&&nocAux.codigo!=codigo) {
                nocAux=nocAux.prox;
            }
            if(nocAux.codigo==codigo) {
                return nocAux;
            } else {
                System.out.println("Categoria não encontrada!");
            }
        }

        return null;
    }

    @Override
    public void alterar(int codigo, String descricao) {
        if(inicio==null) {
            System.out.println("Lista vazia!");
        }else {
            Noc nocAux = inicio;
            while(nocAux.prox!=null&&nocAux.codigo!=codigo) {
                nocAux=nocAux.prox;
            }
            if(nocAux.codigo==codigo) {
                nocAux.descricao=descricao;
            } else {
                System.out.println("Categoria não encontrada!");
            }

        }

    }

    @Override
    public void imprimeTudo() {

        if(inicio==null) {
            System.out.println("Lista vazia!");
        }else {
            Noc nocAux = inicio;
            while(nocAux!=null) {
                System.out.print(nocAux.descricao + " ");
                if(nocAux.ini != null){
                    No noAux = nocAux.ini;
                    while(noAux!=null) {
                        System.out.print(noAux.valor + " ");
                        noAux=noAux.prox;
                    }
                }
                nocAux=nocAux.prox;
                System.out.print(", ");
            }
        }
        System.out.print(" ");
    }



    @Override
    public void imprimeCategoria(int codigo) {
        if(inicio==null) {
            System.out.println("Lista vazia!");
        } else {
            Noc nocAux = inicio;
            while(nocAux.prox!=null&&nocAux.codigo!=codigo) {
                nocAux=nocAux.prox;
            }
            if(nocAux.codigo==codigo) {
                System.out.print(nocAux.descricao+" ");
                if(nocAux.ini!=null){
                    No noAux = nocAux.ini;
                    while(noAux!=null) {
                        System.out.print(noAux.valor + " ");
                        noAux=noAux.prox;
                    }
                }
            } else {
                System.out.println("Categoria não encontrada!");
            }
        }

    }

    @Override
    public void insereNo(int codigo, int valor) {

        if(inicio==null) {
            System.out.println("Lista vazia!");
        } else {
            Noc nocAux = inicio;
            while(nocAux.prox!=null&&nocAux.codigo!=codigo) {
                nocAux=nocAux.prox;
            }
            if(nocAux.codigo==codigo) {
                No noNovo = new No();
                noNovo.valor = valor;

                if(nocAux.ini==null) {
                    nocAux.ini = noNovo;
                } else {
                    No noAux = nocAux.ini;
                    while (noAux.prox != null) {
                        noAux = noAux.prox;
                    }

                    noAux.prox = noNovo;
                }
            } else {
                System.out.println("Categoria não encontrada!");
            }
        }

    }

    @Override
    public void removeNo(int codigo, int valor) {

        if(inicio==null) {
            System.out.println("Lista vazia!");
        } else {
            Noc nocAux = inicio;
            while(nocAux.prox!=null&&nocAux.codigo!=codigo) {
                nocAux=nocAux.prox;
            }
            if(nocAux.codigo==codigo) {
                if(nocAux.ini==null) {
                   System.out.println("Elemento não encontrado!");
                } else {
                    No noAux = nocAux.ini;
                    No noAnt = null;

                    while (noAux.prox != null && noAux.valor != valor) {
                        noAnt = noAux;
                        noAux = noAux.prox;
                    }
                    if(noAux.valor == valor){
                        if(noAnt == null){
                            nocAux.ini = nocAux.ini.prox;
                        } else if (noAux.prox == null){
                            noAnt.prox=null;
                            noAux = null;
                        } else {
                            noAnt.prox = noAux.prox;
                            noAux=null;
                        }
                    } else {
                        System.out.println("Nó não encontrado!");
                    }
                }
            } else {
                System.out.println("Categoria não encontrada!");
            }
        }

    }

    @Override
    public No buscarNo(int codigo, int valor) {
        if(inicio==null) {
            System.out.println("Lista vazia!");
        }else {
            Noc nocAux = inicio;
            while(nocAux.prox!=null&&nocAux.codigo!=codigo) {
                nocAux=nocAux.prox;
            }
            if(nocAux.codigo==codigo) {
                if(nocAux.ini == null){
                    System.out.println("Elemento não encontrado!");
                } else {
                    No noAux = nocAux.ini;
                    while(noAux.prox != null && noAux.valor != valor){
                        noAux = noAux.prox;
                    }
                    if(noAux.valor == valor){
                        return noAux;
                    } else {
                        System.out.println("Elemento não encontrado!");
                    }
                }

            } else {
                System.out.println("Categoria não encontrada!");
            }
        }

        return null;
    }

    @Override
    public void alterarNo(int codigo, int valor, int novoValor) {

        if(inicio==null) {
            System.out.println("Lista vazia!");
        } else {
            Noc nocAux = inicio;
            while(nocAux.prox!=null&&nocAux.codigo!=codigo) {
                nocAux=nocAux.prox;
            }
            if(nocAux.codigo==codigo) {
                No noNovo = new No();
                noNovo.valor = valor;

                if(nocAux.ini==null) {
                    System.out.println("Nó não encontrado!");
                } else {
                    No noAux = nocAux.ini;

                    while (noAux.prox != null && noAux.valor != valor) {
                        noAux = noAux.prox;
                    }
                    if(noAux.valor == valor){
                        noAux.valor = novoValor;
                    } else {
                        System.out.println("Nó não encontrado!");
                    }
                }
            } else {
                System.out.println("Categoria não encontrada!");
            }
        }

    }
}
