package ListaMultidimensional;

public interface ILMD {

    void insere (int codigo, String descricao );
    void remove (int codigo);
    Noc buscar (int codigo);
    void alterar (int codigo, String descricao);
    void imprimeTudo();
    void imprimeCategoria(int codigo);
    void insereNo(int codigo, int valor);
    void removeNo(int codigo, int valor);
    No buscarNo(int codigo,int valor);
    void alterarNo(int codigo, int valor, int novoValor);

}
