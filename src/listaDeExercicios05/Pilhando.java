package listaDeExercicios05;

public class Pilhando {

    public static Pilha pilhando1(){
        Pilha pilha = new Pilha();
        pilha.push(1);
        pilha.push(2);
        pilha.push(5);
        pilha.push(7);
        pilha.push(4);
        return pilha;
    }

    public static Pilha pilhando2(){
        Pilha pilha = new Pilha();
        pilha.push(3);
        pilha.push(2);
        pilha.push(6);
        pilha.push(5);
        return pilha;
    }

    public static Pilha pilhando3() {
        Pilha pilha = new Pilha();
        pilha.push(33);
        pilha.push(2);
        pilha.push(4);
        pilha.push(1);
        pilha.push(88);
        return pilha;
    }

    public static Pilha pilhando4(){
        Pilha pilha = new Pilha();
        pilha.push(1);
        pilha.push(2);
        pilha.push(5);
        pilha.push(7);
        return pilha;
    }
}
