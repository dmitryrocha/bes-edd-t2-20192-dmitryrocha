package listaDeExercicios05.questao04;

public interface IPilhaString {
    void push(char valor);
    NoString pop();
    void imprimir();
}
