package listaDeExercicios05.questao04;

public class PilhaString implements IPilhaString{

    NoString topo = null;

    @Override
    public void push(char valor) {
        NoString novo = new NoString();

        novo.letra = valor;
        novo.prox = topo;
        topo = novo;
    }

    @Override
    public NoString pop() {
        NoString aux = topo;
        if(topo!=null) {
            topo = topo.prox;
        }

        return aux;
    }

    @Override
    public void imprimir() {
        if(topo==null) {
            System.out.println("Pilha vazia!");
        }else {
            NoString aux = topo;
            while(aux!=null) {
                System.out.print(aux.letra + " ");
                aux=aux.prox;
            }
        }
        System.out.println("");
    }
}
