package listaDeExercicios05;

import listaDeExercicios05.questao04.PilhaString;
import listaDeExercicios05.questao04.PilhandoString;

import java.util.ArrayList;

public class Questoes {

    private ArrayList<Character> arrayChar = new ArrayList<Character>();

    public static void main(String[] args) {
        executa(new Pilha());

    }

    public static void executa(Pilha P1) {
        P1 = Pilhando.pilhando1();
        Pilha P2 = Pilhando.pilhando2();

        questao01(P1, P2);

        P1 = Pilhando.pilhando2();
        P2 = Pilhando.pilhando1();

        questao01(P1, P2);

        P1 = Pilhando.pilhando3();
        P2 = Pilhando.pilhando1();

        questao01(P1, P2);

        P1 = Pilhando.pilhando1();
        P1.imprimir();
        P2 = questao02(P1);
        P2.imprimir();

        P1 = Pilhando.pilhando1();
        P2 = Pilhando.pilhando3();

        System.out.println("A resposta deve ser falso: \n"+questao03(P1, P2));

        P1 = Pilhando.pilhando1();
        P2 = Pilhando.pilhando2();

        System.out.println("A resposta deve ser falso: \n"+questao03(P1, P2));

        P1 = Pilhando.pilhando1();
        P2 = Pilhando.pilhando4();

        System.out.println("A resposta deve ser falso: \n"+questao03(P1, P2));

        P1 = Pilhando.pilhando1();
        P2 = Pilhando.pilhando1();

        System.out.println("A resposta deve ser verdadeiro: \n"+questao03(P1, P2));

        System.out.println("Questão 04:");
        PilhaString pilhaString = PilhandoString.pilhadoPalavra1();
        pilhaString.imprimir();

    }


    private static int tamanho(Pilha pilha) {
        int count = 0;
        if (pilha.topo == null) {
            System.out.println("Lista vazia!");
            return 0;
        } else {
            count++;
            while (pilha.pop() != null) {
                count++;

            }
        }
        return count;
    }

    private static void questao01(Pilha P1, Pilha P2) {
        int P1Size = tamanho(P1);
        int P2Size = tamanho(P2);

        if (P1Size > P2Size) {
            System.out.println("A pilha 1 é maior!");
        } else if (P2Size > P1Size) {
            System.out.println("A pilha 2 é maior!");
        } else {
            System.out.println("As duas pilhas são iguais");
        }
    }

    private static Pilha questao02(Pilha p) {
        Pilha aux = new Pilha();

        if (p.topo == null) {
            System.out.println("A pilha está vazia!");
            return null;
        } else {
            while (p.topo.prox != null) {
                aux.push(p.topo.valor);
                p.pop();
            }
            aux.push(p.topo.valor);

            p = aux;
            return p;
        }
    }

    public static boolean questao03(Pilha p1, Pilha p2) {
        if(p1.topo == null || p2.topo == null) {
            System.out.println("Pelo menos uma pilha está vazia");
        } else {
            Pilha aux1 = new Pilha();
            Pilha aux2 = new Pilha();
            while (p1.topo.prox != null) {
                if(p1.topo.valor == p2.topo.valor){
                    p1.pop();
                    p2.pop();
                } else {
                    return false;
                }
            }
            if(p1.topo.valor == p2.topo.valor) {
                p1.pop();
                p2.pop();
            } else if (p2.topo.prox != null) {
                return false;
            }

        }
        return true;
    }

//    public static boolean questao04(String frase){
//        PilhaString pilhaAux = new PilhaString();
//        char[] vetorFrase = frase.toCharArray();
//        int countD = 0;
//        for(int i = 0; i < vetorFrase.length; i++) {
//            if (vetorFrase[i] == 'D') {
//                countD++;
//            }
//        }
//        if(countD == 0) {
//            return false;
//        } else {
//            String[] pedacos = new String[countD];
//            pedacos = frase.split("D");
//            if(pedacos.length % 2 != 0) {
//                return false;
//            } else {
//                for(int i = 0; i < pedacos.length; i++) {
//                    if(i%2==0) {
//                        for (int j = 0; j < pedacos.length; j++) {
//                            arrayChar.add(pedacos[j]);
//                        }
//                    }
//                    }
//                }
//
//            }
//        }
//
//
//
//        return false;
//    }
}
