package listaDeExerciciosAula02;

import java.util.Scanner;

public class Biblioteca {
	
	
	public static Livro[] arrayLivro = new Livro[4];

	public static void main(String[] args) {
		
		operacional();

	}

	private static void operacional() {
		Scanner sc = new Scanner(System.in);
		
		String autor;
		String genero;
		
		//preenche o array
		arrayLivro = LivroConcreto.inserir();
		
		//imprime tudo para verificar se o array está bem preenchico
		LivroConcreto.imprimir(arrayLivro);
		System.out.println("------------------------------------------");
		
		//realiza a busca
		System.out.println("Bem vindo(a) Ao sistema de buscas da Biblioteca.\nVocê deseja buscar pelo livro do Vinicius de Moraes");
		System.out.println("Digite o autor do livro a ser buscado");
		autor = sc.nextLine();
		System.out.println("Agora digite o gênero a ser buscado. (DICA = é poesia)");
		genero = sc.nextLine();
		
		Livro busca = LivroConcreto.buscaEspecifica(arrayLivro,autor, genero);
		
		if(busca != null) {
			System.out.println("A busca encontrou: ");
			System.out.println("Título: " +busca.getTitulo());
			System.out.println("Autor: "+busca.getAutor());
			System.out.println("Gênero: "+busca.getGenero());
			System.out.println("Ano de publicação: "+busca.getAno());
			
		} else {
			System.out.println("Sua busca não retornou um resultado válido.");
		}
		
		
	}

}
