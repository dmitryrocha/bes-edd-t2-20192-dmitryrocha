package listaDeExerciciosAula02;

public class LivroConcreto implements ILivro {
	
	String titulo;
	String autor; 
	String genero;
	int ano;

	//Método que cria objetos Livro 
	
	public static Livro livroCria(String titulo, String autor, String genero, int ano) {
		
		Livro livroC = new Livro();
		
		livroC.titulo = titulo;
		livroC.autor = autor;
		livroC.genero = genero;
		livroC.ano = ano;
		
		return livroC;
	}

	// Método que insere os livros no array
	public static Livro[] inserir() {

		Livro arrayLivro[] = new Livro[4];
		arrayLivro[0] = livroCria("Novos Poemas", "Vinicius de Moraes", "poesia", 1938);
		arrayLivro[1] = livroCria("Poemas Escritos na Índia", "Cecília Meireles", "poesia", 1962);
		arrayLivro[2] = livroCria("Orfeu da Conceição", "Vinicius de Moraes", "teatro", 1954);
		arrayLivro[3] = livroCria("Ariana, a Mulher", "Vinicius de Moraes", "poesia", 1936);
		return arrayLivro;


	}
	
	//Método feito apenas para ver se o array está preenchido corretamente
	public static void imprimir(Livro[] arrayLivro) {
		
		for(int i=0; i<4; i++) {
			System.out.println("Livro "+(i+1));
			System.out.println("Título: "+arrayLivro[i].getTitulo());
			System.out.println("Autor(a): "+arrayLivro[i].getAutor());
			System.out.println("Gênero: "+arrayLivro[i].getGenero());
			System.out.println("Ano de publicação: "+arrayLivro[i].getAno());
			if(livroVerificaModernismo(arrayLivro[i].getAno())==1) {
				System.out.println("Este livro pertence ao segundo período do modernismo brasileiro");
			} else {
				System.out.println("Este livro não pertence ao segundo período do modernismo brasileiro");
			}
			System.out.println("------------------------------------------");
		}
	}
	
	//Método que faz a busca no array
	public static Livro buscaEspecifica (Livro[] arrayLivro, String autor, String genero) {
				
		for(int i=0; i<arrayLivro.length; i++) {

			if(arrayLivro[i].getAutor().equals(autor) && arrayLivro[i].getGenero().equals(genero)) {
				return arrayLivro[i];
			}
		}
		
		return null;
				
	}

	@Override
	public String getTitulo() {

		return titulo;
	}

	@Override
	public String getAutor() {

		return autor;
	}

	@Override
	public String getGenero() {

		return genero;
	}

	@Override
	public int getAno() {

		return ano;
	}

	//Apenas verifica se o ano está dentro do modernismo
	public static int livroVerificaModernismo(int ano) {
		if (ano < 1930 || ano > 1945) {
			return 1;
		} else {
			return 0;
		}

	}
}
