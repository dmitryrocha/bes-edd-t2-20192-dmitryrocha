package listaDeExerciciosAula02;

//Classe Livro determina as variáveis do objeto Livro e deterina os métodos get

public class Livro {
	
	String titulo;
	String autor;
	String genero;
	int ano;
	
	
	public String getTitulo() {
		return titulo;
	}
	public String getAutor() {
		return autor;
	}
	public String getGenero() {
		return genero;
	}
	public int getAno() {
		return ano;
	}

}
