package listaDeExerciciosAula02;

/* Tal qual foi solicitado, a Interface "contrata" 6 métodos 
 * Os métodos que retornam alguma coisa não são escritos como Override 
 *nos métodos seguintes*/

public interface ILivro {
	static Livro livroCria(String titulo, String autor, String genero, int ano) {
		Livro novoLivro = new Livro();
		novoLivro.titulo = titulo;
		novoLivro.autor = autor;
		novoLivro.genero = genero;
		novoLivro.ano = ano;
		return novoLivro;
	}
	public String getTitulo();
	public String getAutor();
	public String getGenero();
	public int getAno();
	static int livroVerificaModernismo(int ano) {
		if (ano < 1930 || ano > 1945) {
			return 1;
		} else {
			return 0;
		}

	}

}
