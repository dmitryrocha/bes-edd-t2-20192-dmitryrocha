package Fila;

public class Teste {
    public static void main(String[] args) {
        Fila filinha = new Fila();
        filinha = enfileirando();

        filinha.imprimir();

        filinha.remover();
        filinha.imprimir();
        filinha.remover();
        filinha.remover();
        filinha.imprimir();
        filinha.remover();
        filinha.imprimir();
    }

    public static Fila enfileirando() {
        Fila f1 = new Fila();
        f1.inserir(3);
        f1.inserir(2);
        f1.inserir(6);
        f1.inserir(33);

        return f1;
    }
}
