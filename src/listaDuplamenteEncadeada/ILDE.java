package listaDuplamenteEncadeada;

public interface ILDE {

    void inserir(int valor);
    void remover(int valor);
    No buscar(int chave);
    void alterar(int chave, int novoValor);
    void imprimirAscendente();
    void imprimirDescendente();

}
