package listaDuplamenteEncadeada;

public class LDE implements ILDE {

    No inicio = null;

    @Override
    public void inserir(int valor) {
        No novo = new No();
        novo.valor = valor;

        if (inicio == null) {
            inicio = novo;
        } else {
            novo.prox = inicio;
            inicio.ant = novo;
            inicio = novo;

        }
    }

    @Override
    public void remover(int chave) {
        if(inicio==null) {
            System.out.println("Lista vazia!");
        }else {
            No ant = null;
            No aux = inicio;
            while(aux.prox!=null&&aux.valor!=chave) {
                aux=aux.prox;
            }
            if(aux.valor==chave) {
                if(aux.ant==null) {
                    inicio=inicio.prox;
                    inicio.ant = null;
                }else if(aux.prox==null) {
                    aux.ant.prox=null;
                    aux.ant = null;
                    aux=null;
                }else {
                    aux.ant.prox = aux.prox;
                    aux.prox.ant = aux.ant;
                    aux.ant = null;
                    aux.prox = null;
                    aux = null;
                }
            }else {
                System.out.println("Elemento não encontrado!");
            }

        }

    }

    @Override
    public No buscar(int chave) {
        if(inicio == null){
            System.out.println("Lista vazia");
        } else {
            No aux = inicio;
            while(aux.prox != null && aux.valor != chave){
                aux = aux.prox;
            }
            if(aux.valor == chave){
                return aux;
            } else {
                System.out.println("Elemento não encontrado");
            }
        }
        return null;
    }

    @Override
    public void alterar(int chave, int novoValor) {
        if(inicio == null){
            System.out.println("Lista vazia");
        } else {
            No aux = inicio;
            while(aux.prox != null && aux.valor != chave){
                aux = aux.prox;
            }
            if(aux.valor == chave) {
                aux.valor = novoValor;
            } else {
                System.out.println("Elemento não encontrado");
            }
        }
        }

    @Override
    public void imprimirAscendente() {
        if(inicio==null) {
            System.out.println("Lista vazia!");
        }else {
            No aux = inicio;
            while (aux != null) {
                System.out.print(aux.valor + " ");
                aux = aux.prox;
            }
        }
    }

    @Override
    public void imprimirDescendente() {
        if (inicio == null) {
            System.out.println("Lista vazia!");
        } else {
            No aux = inicio;
            while (aux.prox != null) {

                aux = aux.prox;
            }
            while (aux != null) {
                System.out.print(aux.valor + " ");
                aux = aux.ant;
            }

        }
    }

}
