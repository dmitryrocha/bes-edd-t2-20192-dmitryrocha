package listaDuplamenteEncadeada;

public class Teste {

    public static void main(String[] args) {

        executa(new LDE());

    }

    public static void executa(ILDE lista) {

        lista.inserir(2);
        lista.inserir(7);
        lista.inserir(0);
        lista.inserir(9);
        lista.inserir(3);

        System.out.println("Esperado: 3 9 0 7 2");
        lista.imprimirAscendente();

        System.out.println("\nEsperado: 2 7 0 9 3");
        lista.imprimirDescendente();

        lista.alterar(2, 32);
        System.out.println("\nEsperado: 3 9 0 7 32");
        lista.imprimirAscendente();

        lista.alterar(3, 93);
        System.out.println("\nEsperado: 93 9 0 7 32");
        lista.imprimirAscendente();

        lista.alterar(0, 50);
        System.out.println("\nEsperado: 93 9 50 7 32");
        lista.imprimirAscendente();

        System.out.println("\nEsperado: 93");
        No no1 = lista.buscar(93);
        if(no1!=null) {
            System.out.println(no1.valor);
        }

        System.out.println("\nEsperado: 32");
        No no2 = lista.buscar(32);
        if(no2!=null) {
            System.out.println(no2.valor);
        }

        System.out.println("\nEsperado: 50");
        No no3 = lista.buscar(50);
        if(no3!=null) {
            System.out.println(no3.valor);
        }

        System.out.println("\nEsperado: Elemento n�o encontrado!");
        No no4 = lista.buscar(115);
        if(no4!=null) {
            System.out.println(no4.valor);
        }

        lista.remover(93);

        System.out.println("\nEsperado: 9 50 7 32");
        lista.imprimirAscendente();

        System.out.println("\nEsperado: 32 7 50 9");
        lista.imprimirDescendente();

        lista.remover(32);

        System.out.println("\nEsperado: 9 50 7");
        lista.imprimirAscendente();

        System.out.println("\nEsperado: 7 50 9");
        lista.imprimirDescendente();

        lista.remover(50);

        System.out.println("\nEsperado: 9 7");
        lista.imprimirAscendente();

        System.out.println("\nEsperado: 7 9");
        lista.imprimirDescendente();

    }
}
