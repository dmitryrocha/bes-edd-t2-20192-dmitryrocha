package listaDeExercicios04;

import java.util.Scanner;

public class Questao03 {
    public static void main(String[] args) {
        executa();
    }

    private static void executa(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Bem vindo(a) ao soma sem soma");
        System.out.println("Favor escorlher o primeiro número a ser somado:");
        int pred = sc.nextInt();
        System.out.println("Agora escolha o segundo número:");
        int suc = sc.nextInt();
        System.out.println("Sua soma resultou: "+sucPred(pred,suc));

    }

    private static int sucPred(int pred, int suc){
        if(suc == 0){
            return pred;
        } else {
            ++pred;
            --suc;
            return sucPred(pred,suc);
        }
    }
}

