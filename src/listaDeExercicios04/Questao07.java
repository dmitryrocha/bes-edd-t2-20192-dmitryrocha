package listaDeExercicios04;

public class Questao07 {
    public static void main(String[] args) {
        executa();
    }

    private static void executa() {
        System.out.println("A sequência de Fibonacci até o termo de ordem 20 é:");
        fibPrint(20);
    }

    private static int fibonacci(int n) {
        int x = 0;
        if(n>0){
            if(n == 1){
                x = 1;
            } else if(n == 2) {
                x = 1;
            } else {
                x = fibonacci(n-1) + fibonacci(n-2);

            }
        }
        return x;
    }

    private static void fibPrint(int n) {

        for(int i = 1; i <= n; i++){
            if(i != n) {
                System.out.print(fibonacci(i) + ", ");
            } else {
                System.out.print(fibonacci(i));
            }
        }

    }
}
