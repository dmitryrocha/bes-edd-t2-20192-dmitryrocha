package listaDeExercicios04;

public class Questao04 {
    public static void main(String[] args) {
        executa();
    }

    private static void executa() {
        System.out.println(mdc(6,10));
    }

    private static int mdc(int x, int y) {
        if(x==y){
            return x;
        }
        else if(y>x){
            y-=x;
            return mdc(y,x);
        } else {
            x-=y;
            return mdc(x,y);
        }
    }
}
