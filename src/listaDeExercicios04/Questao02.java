package listaDeExercicios04;

import java.util.Scanner;

public class Questao02 {

    public static void main(String[] args) {
        executa();
    }

    public static void executa() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite o número inteiro e positivo que você deseja converter para binário");
        int dec = sc.nextInt();
        System.out.println("Seu número em binário é ");
        Dec2Bin(dec);
    }


    public static void Dec2Bin(int num){

        if(num>0){
            Dec2Bin(num /2);
            System.out.print(num % 2);
        }


    }
}
