package listaDeExercicios04;

import java.util.Scanner;

public class Questao06 {
    public static void main(String[] args) {
        executa();
    }

    private static void executa() {
        Scanner sc = new Scanner (System.in);
        System.out.println("Vamos tentar calcular o quociente de uma divisão!\nFavor escolher o dividendo:");
        int x = sc.nextInt();
        System.out.println("Agora escolha um divisor:");
        int y = sc.nextInt();
        System.out.println("A recursiva retornou: "+div(x,y));

    }

    private static int div(int x, int y){
        if(x == y){
            return 1;
        } else if(x < y){
            return 0;
        } else {
            return 1 + div(x-y,y);
        }

    }
}
