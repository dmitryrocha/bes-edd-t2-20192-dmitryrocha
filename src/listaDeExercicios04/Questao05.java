package listaDeExercicios04;

import java.util.Scanner;

public class Questao05 {
    public static void main(String[] args) {
        executa();
    }

    private static void executa() {
        Scanner sc = new Scanner (System.in);
        System.out.println("Vamos tentar calcular o resto de uma divisão!\nFavor escolher o dividendo:");
        int x = sc.nextInt();
        System.out.println("Agora escolha um divisor:");
        int y = sc.nextInt();
        System.out.println("A recursiva retornou: "+mod(x,y));


    }

    private static int mod(int x, int y){
        if(x == y){
            return 0;
        } else if (x < y) {
            return x;
        } else if(x > y) {
            return mod(x-y, y);
        }

        return -1;
    }
}
