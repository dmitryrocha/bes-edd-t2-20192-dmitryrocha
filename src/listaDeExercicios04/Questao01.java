package listaDeExercicios04;

import java.util.Scanner;

public class Questao01 {
    public static void main(String[] args) {
        executa();
    }

    public static void executa() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Favor digitar o valor da base:");
        int x = sc.nextInt();
        System.out.println("Favor difitar o valor da potência:");
        int n = sc.nextInt();
        System.out.println(x +" elevado à "+n+" potência é igual a "+potenciacao(n,x));
    }

    public static long potenciacao(int n, int x) {
        if (n==0)
            return 1;
        if(n==1)
            return x;
        else
            return x*potenciacao(n-1, x);
    }
}
