package listaDeExercicios04;

import java.util.Scanner;

public class Questao08 {
    public static void main(String[] args) {
        executar();
    }

    private static void executar() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Favor o número de inteiros a ser somado:");
        int x = sc.nextInt();
        System.out.println(soma(x));
    }

    private static int soma(int n){

        if(n == 0) {
            return 0;
        } else {
            return n+soma(n-1);
        }

    }
}
