package listaDeExercicios04;

import java.util.Scanner;

public class Questao10 {
    public static void main(String[] args) {
        executa();
    }

    private static void executa() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Qual o 'n' que você quer neste cálculo que o professor inventou?");
        int count = sc.nextInt();


        System.out.println("A soma totaliza: "+calculo(count));
    }

    private static double calculo(int count){
        if(count == 0){

            return 0;

        } else {
//            double d1 = ((double)Math.pow(count-1,2)+1);
//            double d2 = ((double)(count-1)+3);
            //System.out.println("n = "+n+" e count = "+count);
            return ((double)(Math.pow(count,2)+1)/(count+3))+ calculo(count-1);
        }
    }


}
