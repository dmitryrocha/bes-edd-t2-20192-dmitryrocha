package listaDeExercicios04;

import java.util.Scanner;

public class Questao09 {
    public static void main(String[] args) {
        executa();
    }

    private static void executa() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Vamos fazer uma multiplicação através de somas\nQual o primeiro multiplicador?");
        int x = sc.nextInt();
        System.out.println("Qual o segundo multiplicador?");
        int y = sc.nextInt();

        System.out.println("A sua multiplicação resultou: "+multiSoma(x,y));


    }

    private static int multiSoma(int x, int y){
        if(y == 0){
            return 0;
        } else {
            return  x+multiSoma(x,y-1);
        }
    }
}
