
public class LCO implements ILC {

	bckNo inicio = null;
	bckNo fim = null;

	public void inserir(int valor) {
		bckNo novo = new bckNo();
		novo.valor = valor;
		
		if(inicio==null) {
			inicio = novo;
			fim = novo;
			fim.prox = inicio;
		}else {
			bckNo ant = null;
			bckNo aux = inicio;
			while(aux!=fim&&valor>aux.valor) {
				ant=aux;aux=aux.prox;
			}
			if(ant==null&&valor<aux.valor) {
				novo.prox=inicio;
				inicio=novo;
				fim.prox=inicio;
			}else if(aux.prox==inicio&&valor>aux.valor) {
				aux.prox=novo;
				novo.prox=inicio;
				fim = novo;
			}else {
				ant.prox=novo;
				novo.prox=aux;
			}
		}

	}


	public void remover(int chave) {

		if(inicio==null) {
			System.out.println("Lista vazia!");
		}else {
			bckNo ant = null;
			bckNo aux = inicio;
			
			while(aux.prox!=inicio&&aux.valor!=chave) {
				ant=aux;
				aux=aux.prox;
			}
			
			if(aux.valor==chave) {
				
				if(ant==null) {
					inicio=inicio.prox;
					fim.prox=inicio;
					aux.prox=null;
					aux=null;
				}else if(aux.prox==inicio) {
					ant.prox=inicio;
					fim=ant;
					aux.prox=null;
					aux=null;
				}else {
					ant.prox=aux.prox;
					aux.prox=null;
					aux=null;
				}
				
			}else {
				System.out.println("Elemento n�o encontrado!");
			}
		}

	}


	public void alterar(int chave, int novoValor) {
		
		if(inicio==null) {
			System.out.println("Lista vazia!");
		}else {
			bckNo aux = inicio;
			while(aux.prox!=inicio&&aux.valor!=chave) {
				aux = aux.prox;
			}
			if(aux.valor==chave) {
				aux.valor=novoValor;
			}else {
				System.out.println("Elemento n�o encontrado!");
			}
		}
		

	}


	public bckNo buscar(int chave) {

		if(inicio==null) {
			System.out.println("Lista vazia!");
		}else {
			bckNo aux = inicio;
			while(aux.prox!=inicio&&aux.valor!=chave) {
				aux = aux.prox;
			}
			if(aux.valor==chave) {
				return aux;
			}else {
				System.out.println("Elemento n�o encontrado!");
			}
		}

		return null;
	}


	public void imprimir() {

		if(inicio==null) {
			System.out.println("Lista vazia!");
		}else {
			bckNo aux = inicio;
			do {
				System.out.print(aux.valor + " ");
				aux=aux.prox;
			}while(aux!=inicio);
		}
		System.out.println("");
	}


	public void imprimirDuasVezes() {

		if(inicio==null) {
			System.out.println("Lista vazia!");
		}else {
			bckNo aux = inicio;
			int count=0;
			do {
				System.out.print(aux.valor + " ");
				aux=aux.prox;
				if(aux==inicio)
					count++;
			}while(count<2);
		}
		System.out.println("");
	}

}
