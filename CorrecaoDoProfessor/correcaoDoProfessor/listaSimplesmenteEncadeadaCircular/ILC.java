
public interface ILC {

	void inserir(int valor);
	void remover(int chave);
	void alterar(int chave, int novoValor);
	bckNo buscar(int chave);
	void imprimir();
	void imprimirDuasVezes();
	
}
