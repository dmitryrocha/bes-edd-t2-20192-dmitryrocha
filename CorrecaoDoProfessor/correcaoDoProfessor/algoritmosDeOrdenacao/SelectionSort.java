package x.correcaoDoProfessor.algoritmosDeOrdenacao;

public class SelectionSort {
    public static void main(String[] args) {
        int[] valores = {4,1,5,2,9,7};
        imprime(ordena(valores));
    }

    public static int[] ordena(int[] vetor) {
        int i=0, j=0;
        for(i=0;i<vetor.length;i++) {
            int key=i;
            for(j=i; j<vetor.length; j++) {
                if(vetor[j]<vetor[key])
                    key=j;
            }
            int temp=vetor[i];
            vetor[i]=vetor[key];
            vetor[key]=temp;
        }
        return vetor;
    }

    public static void imprime(int[] vetor) {
        for(int i:vetor) {
            System.out.print(i + " ");
        }
    }
}
