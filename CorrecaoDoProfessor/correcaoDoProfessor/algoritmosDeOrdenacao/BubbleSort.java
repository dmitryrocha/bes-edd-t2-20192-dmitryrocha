package x.correcaoDoProfessor.algoritmosDeOrdenacao;

public class BubbleSort {
    public static void main(String[] args) {
        int[] valores = {4,1,5,2,9,7};
        imprime(ordena(valores));
    }

    public static int[] ordena(int[] vetor) {
        for(int i=0;i<vetor.length; i++) {
            for(int j=0;j<vetor.length-1;j++) {
                if(vetor[j]>vetor[j+1]) {
                    int temp=vetor[j];
                    vetor[j]=vetor[j+1];
                    vetor[j+1]=temp;
                }
            }
        }
        return vetor;
    }

    public static void imprime(int[] vetor) {
        for (int i : vetor) {
            System.out.print(i + " ");
        }
    }
}
