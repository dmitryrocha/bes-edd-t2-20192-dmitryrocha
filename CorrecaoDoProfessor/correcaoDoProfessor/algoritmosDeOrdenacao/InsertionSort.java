package x.correcaoDoProfessor.algoritmosDeOrdenacao;

public class InsertionSort {
    public static void main(String[] args) {
        int[] valores = {4,1,5,2,9,7};
        imprime(ordena(valores));
    }

    public static int[] ordena(int[] vetor) {

        for(int i=0;i<vetor.length;i++) {

            for(int j=i; j>0 && vetor[j]<vetor[j-1];j--) {

                int temp=vetor[j-1];
                vetor[j-1]=vetor[j];
                vetor[j]=temp;

            }

        }

        return vetor;

    }

    public static void imprime(int[] vetor) {
        for(int i:vetor) {
            System.out.print(i + " ");
        }
    }
}
