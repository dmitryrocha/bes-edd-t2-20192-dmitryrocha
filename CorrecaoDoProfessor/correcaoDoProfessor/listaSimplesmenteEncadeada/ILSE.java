
public interface ILSE {

	void inserir(int valor);
	void remover(int valor);
	bckNo buscar(int chave);
	void alterar(int chave, int novoValor);
	void imprimir();
}
