
public class TesteLDEO {

	public static void main(String[] args) {

		executa(new LDEO());

	}

	public static void executa(ILDE lista) {
		
		lista.inserir(2);
		lista.inserir(7);
		lista.inserir(1);
		lista.inserir(9);
		lista.inserir(3);
		
		System.out.println("Esperado: 1 2 3 7 9");
		lista.imprimirAscendente();

		System.out.println("Esperado: 9 7 3 2 1");
		lista.imprimirDescendente();
		
		lista.alterar(1, 0);
		System.out.println("Esperado: 0 2 3 7 9");
		lista.imprimirAscendente();

		lista.alterar(3, 4);
		System.out.println("Esperado: 0 2 4 7 9");
		lista.imprimirAscendente();

		lista.alterar(9, 19);
		System.out.println("Esperado: 0 2 4 7 19");
		lista.imprimirAscendente();
		
		System.out.println("Esperado: 0");		
		No no1 = lista.buscar(0);
		if(no1!=null) {
			System.out.println(no1.valor);
		}

		System.out.println("Esperado: 4");		
		No no2 = lista.buscar(4);
		if(no2!=null) {
			System.out.println(no2.valor);
		}

		System.out.println("Esperado: 19");		
		No no3 = lista.buscar(19);
		if(no3!=null) {
			System.out.println(no3.valor);
		}

		System.out.println("Esperado: Elemento n�o encontrado!");		
		No no4 = lista.buscar(115);
		if(no4!=null) {
			System.out.println(no4.valor);
		}

		lista.remover(0);
		
		System.out.println("Esperado: 2 4 7 19");
		lista.imprimirAscendente();

		System.out.println("Esperado: 19 7 4 2");
		lista.imprimirDescendente();

		lista.remover(19);
		
		System.out.println("Esperado: 2 4 7");
		lista.imprimirAscendente();

		System.out.println("Esperado: 7 4 2");
		lista.imprimirDescendente();
		
		lista.remover(4);
		
		System.out.println("Esperado: 2 7");
		lista.imprimirAscendente();

		System.out.println("Esperado: 7 2");
		lista.imprimirDescendente();
		
	}
	
}
