
public class LDE implements ILDE {

	bckNo inicio = null;

	public void inserir(int valor) {

		bckNo novo = new bckNo();
		novo.valor = valor;
		
		if(inicio == null) {
			inicio = novo;
		}else {
			novo.prox = inicio;
			inicio.ant = novo;
			inicio = novo;
		}

	}


	public void remover(int chave) {

		if(inicio == null) {
			System.out.println("Lista vazia!");;
		}else {
			bckNo aux = inicio;
			while(aux.prox!=null&&aux.valor!=chave) {
				aux=aux.prox;
			}
			if(aux.valor==chave) {
				if(aux.ant==null) {
					inicio = inicio.prox;
					inicio.ant = null;
				}else if(aux.prox==null) {
					aux.ant.prox=null;
					aux.ant=null;
					aux=null;
				}else {
					aux.ant.prox=aux.prox;
					aux.prox.ant=aux.ant;
					aux.ant=null;
					aux.prox=null;
					aux=null;
				}
			}else {
				System.out.println("Elemento n�o encontrado!");
			}
		}	

	}


	public bckNo buscar(int chave) {

		if(inicio == null) {
			System.out.println("Lista vazia!");;
		}else {
			bckNo aux = inicio;
			while(aux.prox!=null&&aux.valor!=chave) {
				aux=aux.prox;
			}
			if(aux.valor==chave) {
				return aux;
			}else {
				System.out.println("Elemento n�o encontrado!");
			}
		}
		
		return null;
		
	}


	public void alterar(int chave, int novoValor) {

		if(inicio == null) {
			System.out.println("Lista vazia!");;
		}else {
			bckNo aux = inicio;
			while(aux.prox!=null&&aux.valor!=chave) {
				aux=aux.prox;
			}
			if(aux.valor==chave) {
				aux.valor=novoValor;
			}else {
				System.out.println("Elemento n�o encontrado!");
			}
		}
	}


	public void imprimirAscendente() {

		if(inicio == null) {
			System.out.println("Lista vazia!");;
		}else {
			bckNo aux = inicio;
			while(aux!=null) {
				System.out.print(aux.valor + " ");
				aux=aux.prox;
			}	
			System.out.println("");
		}
		
	}


	public void imprimirDescendente() {

		if(inicio == null) {
			System.out.println("Lista vazia!");;
		}else {
			bckNo aux = inicio;
			while(aux.prox!=null) {
				aux=aux.prox;				
			}
			while(aux!=null) {
				System.out.print(aux.valor + " ");
				aux=aux.ant;				
			}
			System.out.println("");			
		}
		
	}

}
