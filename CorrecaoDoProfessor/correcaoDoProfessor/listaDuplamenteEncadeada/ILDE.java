
public interface ILDE {

	void inserir(int valor);
	void remover(int chave);
	bckNo buscar(int chave);
	void alterar(int chave, int novoValor);
	void imprimirAscendente();
	void imprimirDescendente();	
	
}
